import java.util.List;
import java.util.Scanner;
import java.util.ArrayList;


/**
 * @(#) Menu.java
 */
public class Menu
{
	private List<MainDish> dishes;

	private List<Beverage> beverages;

	public Menu() {
        dishes = new ArrayList<MainDish>();
        beverages = new ArrayList<Beverage>();
    }
	
	
	public List<Beverage> getBeverages(){
		return beverages;
	}
	
	public List<MainDish> getDishes(){
		return dishes;
	}
	
	public void setMenu() {
		
		Scanner in = new Scanner(System.in);	
		System.out.println("\n\nSetting Restaurant Menu");
		System.out.println("======================================");
		
		System.out.println("Setting Dishes Details");
		System.out.println("====================\n");
		
		for (int i = 0; i < 5; i++){
			dishes.add(i, new MainDish());
			System.out.println("\nSetting dish number "+(i+1));
			System.out.println("--------------\n");
			MainDish mainDish = dishes.get(i);
			setDishDetails(in, mainDish);
		}
		
		System.out.println("\nSetting Dishes Prices");
		System.out.println("||||||||||\n");
		setDishePrices(in);
		
		System.out.println("\n\nSetting Beverages");
		System.out.println("====================\n");
		
		in.nextLine();
		
		for (int i = 0; i < 5; i++){
			beverages.add(i, new Beverage());
			System.out.println("\nSetting beverage number "+(i+1));
			System.out.println("--------------\n");
			Beverage beverage = beverages.get(i);
			setBeverageDetails(in, beverage);
		}
		
		System.out.println("\nSetting Beverages Prices");
		System.out.println("||||||||||\n");
		setBeveragePrices(in);

	}
	
//	private void setBeveragePrices(Scanner in) {
//		System.out.println("Write price for LOW quality beverages.");			
//	    int price = 50;
//	    System.out.println("Write price for HIGH quality beverages.");			
//	    int price2 = 70;
//	    
//		for (int i=0; i<5; i++){
//			if (beverages.get(i).qualityLevel == Quality.low)
//				beverages.get(i).setPrice(price);
//			else
//				beverages.get(i).setPrice(price2);
//		}
//	}
	
	private void setBeveragePrices( Scanner in ) {
		System.out.println("Write price for LOW quality beverages.");			
	    int price = in.nextInt();
	    System.out.println("Write price for HIGH quality beverages.");			
	    int price2 = in.nextInt();
	    
		for (int i=0; i<5; i++){
			if (beverages.get(i).qualityLevel == Quality.low)
				beverages.get(i).setPrice(price);
			else
				beverages.get(i).setPrice(price2);
		}
	}

//	private void setBeverageDetails(Scanner in, Beverage beverage) {
//		System.out.println("Write beverage name.");			
//		String beverageName = "cider";
//	    beverage.setName(beverageName);
//	    System.out.println("Write volume for the beverage.");
//	    int beverageVolume = 30;
//	    beverage.setVolume(beverageVolume);
//	    System.out.println("Choose quality for the beverage. (low OR high)");
//	    String beverageQuality = "high"; 
//	    if (beverageQuality == "high")
//	    	beverage.setQuality(Quality.high);
//	    else
//	    	beverage.setQuality(Quality.low);
//		
//	}
	
	private void setBeverageDetails( Scanner in, Beverage beverage ) {
		System.out.println("Write beverage name.");			
		String beverageName = in.nextLine();
	    beverage.setName(beverageName);
	    System.out.println("Write volume for the beverage.");
	    int beverageVolume = in.nextInt();
	    beverage.setVolume(beverageVolume);
	    System.out.println("Choose quality for the beverage. (low OR high)");
	    in.nextLine(); 
	    String beverageQuality = in.nextLine(); 
	    if (beverageQuality == "high")
	    	beverage.setQuality(Quality.high);
	    else
	    	beverage.setQuality(Quality.low);
		
	}

//	private void setDishePrices(Scanner in) {
//		System.out.println("Write price for LOW quality dishes.");			
//	    int price = 50;
//	    System.out.println("Write price for HIGH quality dishes.");			
//	    int price2 = 70;
//	    
//		for (int i=0; i<5; i++){
//			if (dishes.get(i).qualityLevel == Quality.low)
//				dishes.get(i).setPrice(price);
//			else
//				dishes.get(i).setPrice(price2);
//		}
//	}

	private void setDishePrices( Scanner in ) {
		System.out.println("Write price for LOW quality dishes.");			
	    int price = in.nextInt();
	    System.out.println("Write price for HIGH quality dishes.");			
	    int price2 = in.nextInt();
	    
		for (int i=0; i<5; i++){
			if (dishes.get(i).qualityLevel == Quality.low)
				dishes.get(i).setPrice(price);
			else
				dishes.get(i).setPrice(price2);
		}
	}

//	private void setDishDetails(Scanner in2, MainDish dish) {
//		System.out.println("Write dish name.");			
//		String dishName = "Some dish";
//	    dish.setName(dishName);
//	    System.out.println("Write amount of calories for the dish.");
//	    int dishCalorie = 50;
//	    dish.setCalories(dishCalorie);
//	    System.out.println("Choose quality for the dish. (low OR high)"); 
//	    String dishQuality = "high"; 
//	    if (dishQuality == "high")
//	    	dish.setQuality(Quality.high);
//	    else
//	    	dish.setQuality(Quality.low);
//	}	

	private void setDishDetails( Scanner in2, MainDish dish ) {
		System.out.println("Write dish name.");			
		String dishName = in2.nextLine();
	    dish.setName(dishName);
	    System.out.println("Write amount of calories for the dish.");
	    int dishCalorie = in2.nextInt();
	    dish.setCalories(dishCalorie);
	    System.out.println("Choose quality for the dish. (low OR high)");
	    in2.nextLine(); 
	    String dishQuality = in2.nextLine(); 
	    if (dishQuality == "high")
	    	dish.setQuality(Quality.high);
	    else
	    	dish.setQuality(Quality.low);
	}
	
}
