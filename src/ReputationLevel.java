/**
 * @(#) ReputationLevel.java
 */
public enum ReputationLevel
{
	low,
	
	medium,
	
	high
}
