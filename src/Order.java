import java.util.Date;

/**
 * @(#) Order.java
 */
public class Order
{
	protected Date time;
	
	protected String orderNo;
	
	protected int clientSatisfaction;
	
	protected Beverage beverage;
	
	protected MainDish maindish;
	
	protected Table table;
	
	protected Client client;
	
}
