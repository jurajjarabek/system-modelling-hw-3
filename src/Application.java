import java.io.IOException;


public class Application {

	static GameController gameController;
	
	public static void main(String[] args) {

		// TODO play with design
		System.out.println("Welcome to the Game\n\n");

		System.out.println("MAIN MANU");
		System.out.println("Press following letters to select an option\n");

		System.out.println("a - New Game");
		System.out.println("b - Credits");
		System.out.println("c - Quit");
		
		char input = 'd';
		
		try {
			input = (char) System.in.read();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		switch (input){
			case 'a':
				gameController = new GameController();
				gameController.run();
				break;
				
			case 'b':
				// TODO show our names nicely and maybe let the user go back and not quit the game
				System.out.println("Juraj Jarabek, Anastasiia Konoplina");
				break;
				
			case 'c':
				System.out.println("The game is closing.\nGood Bye");
				
				return;
				
			default:
				break;
		}
		
	}

}
