import java.util.List;
import java.util.Date;
import java.util.Scanner;
import java.util.Random;
import java.io.IOException;
import java.util.ArrayList;

/**
 * @(#) Restaurant.java
 */
public class Restaurant
{
protected Random random = new Random();
	
protected String name;

private String address;

private String city;

protected int budget;

private int reputationPoints;

private Barman barman;

private Chef chef;
	
private MenuItem menuItem;
	
private List<Order> order;
	
private List<Table> tables;

private List<Waiter> waiters;

private Table table;

private Menu menu;
	
	public void setName( String restaurantName ) {
		name = restaurantName;
	}

	public void setAdress( String restaurantAdress ) {
		address = restaurantAdress;
	}

	public void setCity( String restaurantCity ) {
		city = restaurantCity;
	}

	public void setBudget( int i ) {
		budget = i;
	}

	public void generateTables( ) {
		
		//TODO take in consideration reputation of the restaurant
		
		tables = new ArrayList<Table>();
		for (int i=0; i < 9; i++){
			tables.add(i, new Table(i+1));
		}
	}

	public void generateStaff( ) {
		// generate 1 chef, 1 barman, 3 waiters
		// set LOE for each person, set salary
		
		chef = new Chef();
		chef.setName("Juraj");
		chef.setSurname("Jarabek");
		chef.taxCode(12345);
		chef.setExperience(Experience.low);
		chef.setSalary();
		
		barman = new Barman();
		barman.setName("Michal");
		barman.setSurname("Tree");
		barman.setExperience(Experience.low);
		barman.setSalary();
		
		waiters = new ArrayList<Waiter>();
		waiters.add(0, new Waiter("John","Blala"));
		waiters.add(1, new Waiter("Matt","Trala"));
		waiters.add(2, new Waiter("Paul","Laala"));
		for (int i = 0; i < 3; i++){
			waiters.get(i).setExperience(Experience.low);
			waiters.get(i).setSalary();			
		}
	}

	public void setReputation( int i ) {
		reputationPoints = i;
	}

	public void setMenu( ) {
		menu = new Menu();
		menu.setMenu();
	}

	
	public void assignTables( ) {
	
		Scanner in = new Scanner(System.in);	
		System.out.println("\n\nAssigning Tables to Waiters (FROM 1 - 9)");
		System.out.println("======================================");

		System.out.println("Rember to choose table which you haven't chosen to other waiter before");
		System.out.println("======================================\n");
		
		int number = 0;
		
		List<Integer> numbers = new ArrayList<Integer>();
		
		
		for (int i = 0; i<3 ; i++){
			// write choosing table for waiter 
			System.out.println("\n\nAssigning table to "+waiters.get(i).name);
			System.out.println("--====--\n");
			for (int j = 0; j<3; j++){
				
				System.out.println("Choose table "+(j+1));
				System.out.println("--------");
				
				do {
					number = in.nextInt();
					if (numbers.contains(number))
						System.out.println("Sorry table is already assigned, choose again");
					
				} while (numbers.contains(number));
		
				numbers.add(number);
				
				waiters.get(i).assignTable(j,tables.get(number-1));
			}
		}
		
		
		
	}

	public int getReputation( ) {
		return reputationPoints;
	}

	
	
	public void runDay( int numberOfCustomers, int day ) {
		
		int costs    = 0;
		int earnings = 0;
		int total = 0;
		
		// generate List of new orders   (AS MANY As numberOfCustomers)
		for (int number = 1; number <= numberOfCustomers; number++)
		{
			costs = 0;
			earnings = 0;
			
			Order order = new Order();
			order.time = new Date(day); //TODO save right date 
			order.orderNo = (""+day+""+number);
			
			
		    // calculate satisfaction level (food quality, service, waiter level etc etc..)
			order.clientSatisfaction = 20; // TODO calculate correct value
			
			order.beverage = menu.getBeverages().get(random.nextInt(5));
			
			order.maindish = menu.getDishes().get(random.nextInt(5));
			
			//TODO check if table wasn't occupied more then twice per
			order.table = tables.get(random.nextInt(numberOfCustomers/2));
			
			
			order.client = new Client();
			// TODO fill client info
			
			earnings = order.beverage.price + order.maindish.price;
			costs = getBeverageCost(order.beverage) + getDishCost(order.maindish); 
			total += earnings - costs;
		}
		
		// increase / decrease budget at the end of the day
		System.out.println("\n\nYOU EARNED "+total+" on "+day+". day"); 
		budget += total;  
		
	}

	private int getDishCost( MainDish maindish ) {
		switch (maindish.qualityLevel){
		case low:
			return 3;
		case high:
			return 10;
		}
		return 3;
	}

	private int getBeverageCost( Beverage beverage ) {
		switch (beverage.qualityLevel){
		case low:
			return 1;
		case high:
			return 3;
		}
		return 1;
	}

	public void trainEmployees( ) {
		
		Scanner in = new Scanner(System.in);
		
		do {
			
			// give option of training employee YES/NO
			System.out.println("Select who do you want to train");
			System.out.println("waiter - Waiter");
			System.out.println("chef   - Chef");
			System.out.println("barman - Barman");

			String input = in.nextLine();		
				
			switch (input){
				case "waiter":
					trainWaiter();
					break;
				case "chef":
					trainChef();
					break;
				case "barman":
					trainBarman();
					break;	
				default :
					System.out.println("You didn't select any of these 3.");
					break;
			}
			
		} while (trainSomeOneElse());
		
		
	}

	private void trainWaiter( ) {
		// TODO Auto-generated method stub
		
		int number = getWaiterNumber();
		if (number == 4)
			return;
		
		if (waiters.get(number).experience == Experience.high){
			System.out.println("You cannot train your Waiter because he has already HIGH LOE.\n");
			return;
		}
		if (budget < 800){
			System.out.println("You cannot train your Waiter since your budget is below 800.\n");
			return;
		} 
		budget -= 800;
		if (waiters.get(number).experience == Experience.low)
			waiters.get(number).experience = Experience.medium;
		else
		if (waiters.get(number).experience == Experience.medium)
			waiters.get(number).experience = Experience.high;
		
		System.out.println("Perfect, Training was SUCESSFULL\n");
	}

	
	
	private int getWaiterNumber( ) {
		
		String first  = waiters.get(0).name;
		String second = waiters.get(1).name;
		String third  = waiters.get(2).name;
		
		System.out.println("Select who do you want to train");
		System.out.println(first);
		System.out.println(second);
		System.out.println(third);
		
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
			
		
		if (input.endsWith(first))
			return 0;
		else
			if (input.endsWith(second))
				return 1;
			else
				if (input.endsWith(third))
					return 2;
				else
					System.out.println("You didn't select any of these 3.");

		return 4;
	}

	private void trainChef( ) {
		if (chef.experience == Experience.high){
			System.out.println("You cannot train your Chef because he has already HIGH LOE.\n");
			return;
		}
		if (budget < 1200){
			System.out.println("You cannot train your Chef since your budget is below 1200.\n");
			return;
		} 
		budget -= 1200;
		if (chef.experience == Experience.low)
			chef.experience = Experience.medium;
		else
		if (chef.experience == Experience.medium)
			chef.experience = Experience.high;
		System.out.println("Perfect, Training was SUCESSFULL\n");
	}

	
	private void trainBarman( ) {
		if (barman.experience == Experience.high){
			System.out.println("You cannot train your Barman because he has already HIGH LOE.\n");
			return;
		}
		if (budget < 1200){
			System.out.println("You cannot train your Barman since your budget is below 1200.\n");
			return;
		} 
		budget -= 1200;
		if (barman.experience == Experience.low)
			barman.experience = Experience.medium;
		else
		if (barman.experience == Experience.medium)
			barman.experience = Experience.high;
		System.out.println("Perfect, Training was SUCESSFULL\n");
	}

	
	private boolean trainSomeOneElse( ) {
		
		System.out.println("Do you want to train someone else?");
		System.out.println("yes - YES");
		System.out.println("no  - NO");
		
		Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		
		if (input.endsWith("yes"))
			return true;
		
		return false;
	}

	
	
}
